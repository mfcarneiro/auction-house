defmodule Auction.Item do
  use Ecto.Schema

  alias Ecto.Changeset

  schema "items" do
    field(:title, :string)
    field(:description, :string)
    field(:ends_at, :utc_datetime)

    timestamps()
  end

  # Default changeset pattern
  def changeset(item, params \\ %{}) do
    item
    |> Changeset.cast(params, [:title, :description, :ends_at])
    |> Changeset.validate_required(:title)
    |> Changeset.validate_length(:title, min: 3)
    |> Changeset.validate_required(:description)
    |> Changeset.validate_length(:description, max: 200)
    |> Changeset.validate_required(:ends_at)
    |> Changeset.validate_change(:ends_at, &validate_future_date/2)
  end

  # Custom method to validate on changeset
  defp validate_future_date(:ends_at, ends_at_future_date) do
    case DateTime.compare(ends_at_future_date, DateTime.utc_now()) do
      :lt -> [ends_at: "ends_at cannot be in the past"]
      _ -> []
    end
  end
end
