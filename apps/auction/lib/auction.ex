defmodule Auction do
  alias Auction.{Repo, Item}

  @repo Repo

  def list_items, do: @repo.all(Item)

  def get_item(id), do: @repo.get!(Item, id)

  def get_item_by(attrs), do: @repo.get_by(Item, attrs)

  def update_item(%Item{} = item, updates),
    do:
      item
      |> Item.changeset(updates)

  # Will return {:ok, struct } or {:error, changeset}
  def insert_item(attrs),
    do:
      Item
      |> struct(attrs)
      |> @repo.insert()

  def delete_item(%Item{} = item), do: @repo.delete(item)
end
